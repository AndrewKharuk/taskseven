﻿using System;
using TaskSeven.Implementations;
using TaskSeven.Interfaces;

namespace TaskSeven
{
    class Program
    {
        static string url = "https://docs.microsoft.com/ru-ru/";
        static string regForUrl = @"(http|ftp|https)://[\w-]+(\.[\w-]+)+([\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-])?";

        static void Main(string[] args)
        {
            IProcessor processor = new UrlProcessor(url, regForUrl);
            processor.WriteDataList();

            Console.ReadKey();
        }
    }
}
