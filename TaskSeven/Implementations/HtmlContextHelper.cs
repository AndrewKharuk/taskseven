﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text.RegularExpressions;
using TaskSeven.Interfaces;

namespace TaskSeven.Implementations
{
    public class HtmlContextHelper : IContextHelper
    {
        private readonly string _url;

        public HtmlContextHelper(string url)
        {
            _url = url;
        }

        public string GetTextFromContext()
        {
            string htmlCode;

            using (WebClient client = new WebClient())
            {
                htmlCode = client.DownloadString(_url);
            }
            return htmlCode;
        }
    }
}
