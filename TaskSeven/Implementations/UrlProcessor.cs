﻿using System.Collections.Generic;
using TaskSeven.Interfaces;

namespace TaskSeven.Implementations
{
    public class UrlProcessor : IProcessor
    {
        private readonly IContextHelper _htmlHelper;
        private readonly IParser _parser;
        private readonly IWriter _writer;

        private readonly string _regForUrl;

        public UrlProcessor(string url, string regforUrl)
        {
            _htmlHelper = new HtmlContextHelper(url);
            _parser = new StringsParser();
            _writer = new ConsoleWriter();
            _regForUrl = regforUrl;
        }

        public void WriteDataList()
        {
            string htmlText = _htmlHelper.GetTextFromContext();
            IEnumerable<string> urlList = _parser.GetStringsListByRegex(_regForUrl, htmlText);
            _writer.WriteFromList(urlList);
        }
    }
}
