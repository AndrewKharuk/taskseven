﻿using System;
using System.Collections.Generic;
using TaskSeven.Interfaces;

namespace TaskSeven.Implementations
{
    public class ConsoleWriter : IWriter
    {
        public void WriteFromList(IEnumerable<string> list)
        {
            foreach (var item in list)
            {
                Console.WriteLine($"{item}\n");
            }
        }
    }
}
