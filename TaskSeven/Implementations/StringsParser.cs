﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using TaskSeven.Interfaces;

namespace TaskSeven.Implementations
{
    public class StringsParser : IParser
    {
        public IEnumerable<string> GetStringsListByRegex(string reg, string text)
        {
            Regex regex = new Regex(reg);

            var matches = regex.Matches(text);

            ICollection<string> urlList = new List<string>();

            foreach (Match item in matches)
            {
                urlList.Add(item.Value);
            }

            return urlList;
        }
    }
}
