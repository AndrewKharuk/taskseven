﻿using System.Collections.Generic;

namespace TaskSeven.Interfaces
{
    public interface IParser
    {
        IEnumerable<string> GetStringsListByRegex(string regex, string text);
    }
}
