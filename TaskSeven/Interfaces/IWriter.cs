﻿using System.Collections.Generic;

namespace TaskSeven.Interfaces
{
    public interface IWriter
    {
        void WriteFromList(IEnumerable<string> list);
    }
}
