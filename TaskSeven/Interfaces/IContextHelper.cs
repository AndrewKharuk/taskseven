﻿namespace TaskSeven.Interfaces
{
    public interface IContextHelper
    {
        string GetTextFromContext();
    }
}
